#ifndef __COMPLETE_H__
#define __COMPLETE_H__

int complete(char *instr, const char **outstr, int *num_chars);

#endif
