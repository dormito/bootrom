////////////////////////////////////////////////////////////////////////////////
///
/// @file       tests.cpp
///
/// @project
///
/// @brief      Main test wrapper.
///
////////////////////////////////////////////////////////////////////////////////
///
////////////////////////////////////////////////////////////////////////////////
///
/// @copyright Copyright (c) 2019-2021, Evan Lojewski
/// @cond
///
/// All rights reserved.
///
/// Redistribution and use in source and binary forms, with or without
/// modification, are permitted provided that the following conditions are met:
/// 1. Redistributions of source code must retain the above copyright notice,
/// this list of conditions and the following disclaimer.
/// 2. Redistributions in binary form must reproduce the above copyright notice,
/// this list of conditions and the following disclaimer in the documentation
/// and/or other materials provided with the distribution.
/// 3. Neither the name of the copyright holder nor the
/// names of its contributors may be used to endorse or promote products
/// derived from this software without specific prior written permission.
///
////////////////////////////////////////////////////////////////////////////////
///
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
/// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
/// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
/// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
/// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
/// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
/// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
/// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
/// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
/// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
/// POSSIBILITY OF SUCH DAMAGE.
/// @endcond
////////////////////////////////////////////////////////////////////////////////

#include <gtest/gtest.h>
#include <OptionParser.h>

#include <vector>

#include "test_support.h"

using std::vector;
using std::cout;
using optparse::OptionParser;
using testing::GTEST_FLAG(list_tests);

#define _STRINGIFY(__STR__) #__STR__
#define STRINGIFY(__STR__) _STRINGIFY(__STR__)

#define VERSION_STRING STRINGIFY(VERSION_MAJOR) "." STRINGIFY(VERSION_MINOR) "." STRINGIFY(VERSION_PATCH)

#define DEFAULT_SERIAL_DESCRIPTION "FTDI Lattice ECP5_5G VERSA Board"

serial::Serial *gSerialPort;


serial::PortInfo enumerate_ports(std::string target_description)
{
	vector<serial::PortInfo> devices_found = serial::list_ports();

	vector<serial::PortInfo>::iterator iter = devices_found.begin();

	while( iter != devices_found.end() )
	{
		serial::PortInfo device = *iter++;

        if (device.description.rfind(target_description, 0) == 0)
        {
            return device;
        }
	}

    return {NULL, NULL, NULL};
}

GTEST_API_ int main(int argc, char **argv) {
    OptionParser parser = OptionParser().description("bootrom tester v" VERSION_STRING);
    parser.version(VERSION_STRING);

    parser.add_option("-s", "--serial")
        .dest("serial")
        .help("Serial port to use for testing")
        .metavar("SERIAL");

    parser.add_option("-d", "--desc")
        .dest("desc")
        .help("Serial Port description to find when autodetecting a serial port (default: %default)")
        .metavar("DESC")
        .set_default(DEFAULT_SERIAL_DESCRIPTION);

    parser.add_option("-b", "--baud")
        .dest("baud")
        .help("Baud rate to communicate with the bootrom. (default: %default)")
        .metavar("BAUD")
        .set_default(115600);

    parser.add_option("-r", "--reload")
        .dest("reload_cmd")
        .help("Command used to reload the FPGA. (default: %default)")
        .metavar("RELOAD_CMD");



    // Parse googletest specific arguments.
    testing::InitGoogleTest(&argc, argv);


    if (GTEST_FLAG(list_tests))
    {
        // No need to parse remaining arguments.
        return RUN_ALL_TESTS();
    }

    // Parse additional arguments
    optparse::Values options = parser.parse_args(argc, argv);
    vector<std::string> args = parser.args();

    std::string serial_port;
    if(options.is_set("serial"))
    {
        serial_port = options["serial_port"];
    }
    else
    {
        // Find possible serial targets
        serial::PortInfo port = enumerate_ports(options["desc"]);

        if(!port.port.empty())
        {
            serial_port = port.port;
        }
    }

    if(serial_port.empty())
    {
        parser.error("No serial port found.");
    }
    else
    {
        std::cout << "Using " << serial_port << std::endl;
    }


    gSerialPort = new serial::Serial(serial_port, options.get("baud"), serial::Timeout::simpleTimeout(5000));

    if (!gSerialPort->isOpen())
    {
        parser.error("Unable to open serial port.");
    }

    if (options.is_set("reload_cmd"))
    {
        std::cout << "Executing " << options["reload_cmd"] << std::endl;
        if (0 != system(options["reload_cmd"].c_str()))
        {
            parser.error("Unaexpected exit status.");
        }
    }

    return RUN_ALL_TESTS();
}
